﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IntegratedProjectsDemo.Entities;

namespace IntegratedProjectsDemo.Repositories
{
    public interface IRepository<T> where T: IEntity
    {
	    Task<T> GetAsync(string id);
	    Task<IEnumerable<T>> GetAllAsync();
	    Task<T> SaveAsync(T entity);
	    Task UpdateAsync(T entity);
	    Task DeleteAsync(string id);
    }
}

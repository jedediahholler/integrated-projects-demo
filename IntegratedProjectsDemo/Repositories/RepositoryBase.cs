﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IntegratedProjectsDemo.Entities;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace IntegratedProjectsDemo.Repositories
{
    public abstract class RepositoryBase<T> : IRepository<T> where T: IEntity
    {
	    protected IMongoCollection<T> Collection;

	    protected RepositoryBase()
	    {
		    var client = new MongoClient("mongodb://<project-demo-user:e3j97w342017@ds157702.mlab.com:57702");
		    var db = client.GetDatabase("project-demo");
		    Collection = db.GetCollection<T>(typeof(T).Name.ToLowerInvariant());
	    }

	    public virtual Task<T> GetAsync(string id)
	    {
		    return Collection.AsQueryable().Where(x => x.Id == id).SingleOrDefaultAsync();
	    }

	    public virtual Task<IEnumerable<T>> GetAllAsync()
	    {
		    return Task.FromResult(Collection.AsQueryable().ToEnumerable());
	    }

	    public virtual async Task<T> SaveAsync(T entity)
	    {
		    entity.Created = DateTime.UtcNow;
		    await Collection.InsertOneAsync(entity, new InsertOneOptions {BypassDocumentValidation = true});
		    return entity;
	    }

	    public virtual async Task UpdateAsync(T entity)
	    {
		    var filter = Builders<T>.Filter.Eq("_id", entity.Id);
			entity.Modified = DateTime.UtcNow;
		    await Collection.ReplaceOneAsync(filter, entity);
	    }

	    public virtual async Task DeleteAsync(string id)
	    {
		    var filter = Builders<T>.Filter.Eq("_id", id);
		    await Collection.DeleteOneAsync(filter);
	    }
    }
}

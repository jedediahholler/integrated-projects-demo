﻿namespace IntegratedProjectsDemo.Entities
{
    public class TodoItem : EntityBase
    {
		public string Title { get; set; }
		public bool Complete { get; set; }
    }
}

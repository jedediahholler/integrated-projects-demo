﻿using System.Collections.Generic;

namespace IntegratedProjectsDemo.Entities
{
    public class TodoList : EntityBase
    {
		public string Title { get; set; }
		public IList<TodoItem> Items { get; set; }
    }
}

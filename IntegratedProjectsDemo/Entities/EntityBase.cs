﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace IntegratedProjectsDemo.Entities
{
    public abstract class EntityBase : IEntity
    {
		[BsonId(IdGenerator = typeof(CombGuidGenerator))]
	    public string Id { get; set; }

	    public DateTime Created { get; set; }
	    public DateTime? Modified { get; set; }
    }
}
